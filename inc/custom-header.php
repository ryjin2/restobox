<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package restobox
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses restobox_header_style()
 */
function restobox_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'restobox_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 2000,
		'height'                 => 500,
		'flex-height'            => true,
		'wp-head-callback'       => 'restobox_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'restobox_custom_header_setup' );

if ( ! function_exists( 'restobox_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see restobox_custom_header_setup().
	 */
	function restobox_header_style() {
		
		if( get_header_image() && is_front_page()){
			?>
			<style type = "text/css">

				.intro-banner{
					background-image: url(<?php echo get_header_image(); ?>);
					padding-top:340px;
					background-attachment: relative;
					background-position: center center fixed;
					background-repeat: no-repeat;
					min-height: 1000px !important;
					min-width: 100% !important;
					height: auto;
					background-size:cover;
					left:0;
				}

			</style>
			<?php
		}

		// If we get this far, we have custom styles. Let's do this.
		?>
				<style type = "text/css">
					.our-gallery{
						background-image:url(<?php bloginfo('template_url'); ?>/images/our-gallery-bg.jpg);
						padding-top:200px;
						background-attachment: relative;
						background-position: center center;
						background-repeat: no-repeat;
						min-height: 600px !important;
						min-width: 100% !important;
						height: auto;
						background-size:cover;
						left:0;
					}
					.contact-us{
						background-image:url(<?php bloginfo('template_url'); ?>/images/contact-us-background-image.jpg);
						padding-top:230px;
						background-attachment: relative;
						background-position: center center;
						background-repeat: no-repeat;
						min-height: 600px !important;
						min-width: 100% !important;
						height: auto;
						background-size:cover;
						left:0;
					}
				</style>
		<style type="text/css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
		?>
			.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}
		<?php
			// If the user has set a custom color for the text use that.
			else :
		?>
			.site-title a,
			.site-description {
				color: #<?php echo esc_attr( $header_text_color ); ?>;
			}
		<?php endif; ?>
		</style>
		<?php
	}
endif;
