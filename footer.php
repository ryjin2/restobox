<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package restobox
 */

?>

	

	<footer class = "resto-footer">
		<div class = "container">
			<div class = "row">
				<div class = "col-md-12 text-center">
					<h1 class = "footer-logo">Restobox</h1>
				</div>
			</div>
			<div class = "row">
				<div class = "col-md-12 text-center">
					<!-- <ul class = "nav justify-content-center">
						<li class = "nav-item"><a href="#" class = "nav-link">All</a></li>
						<li class = "nav-item"><a href="#" class = "nav-link">Dessert</a></li>
						<li class = "nav-item"><a href="#" class = "nav-link">Breakfast</a></li>
						<li class = "nav-item"><a href="#" class = "nav-link">Lunch</a></li>
						<li class = "nav-item"><a href="#" class = "nav-link">Dinner</a></li>
					</ul> -->
					<?php
					
					wp_nav_menu(array(
						'theme_location' => 'Primary Menu',
						'container' => 'ul',
						'menu_class' => 'nav justify-content-center'
					));
					
					?>
				</div>
			</div>
			<div class = "row">
				<div class = "col-md-12 text-center">
					<a href="#"><i class = "fa fa-facebook socials"></i></a>
					<a href="#"><i class = "fa fa-instagram socials"></i></a>
					<a href="#"><i class = "fa fa-twitter socials"></i></a>
				</div>
			</div>
			<div class = "row">
				<div class = "col-md-12 text-center">
					<hr>
					<h1 class = "restobox-inc">© 2017 Restobox Restaurant Inc.</h1>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
