
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package restobox
 */

get_header(); ?>

<div class = "intro-banner">
    <div class = "container">
        <div class = "row">
            <div class = "col-sm-12 text-center">
                <h1 class = "restobox-title">Restobox</h1>
                <p class = "restobox-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie velit ut luctus faucibus. Nulla placerat semper erat quis tempor. Curabitur rhoncus odio tincidunt dui cursus ultricies.</p>
                <a href="<?php echo get_permalink(get_page_by_title('contact us')); ?>" class = "btn btn-default">Book a Table</a>
            </div>
        </div>
    </div>
</div>
    
<div class = "about-us">
    <div class = "container">
        <!-- <div class = "row">
            <div class = "col-sm-12 text-center">
                <h1 class = "about-us-title">About us</h1>
            </div>
        </div> -->
        <div class = "row" id = "discover-margin">
            <div class = "col-sm-5" id = "discover-resto-margin">
                <h1 class = "best-restaurant">Best Restaurant in the town</h1>
                <h2 class = "discover">Discover What Restobox Is</h2>
                <p class = "discover-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur adipisci neque blanditiis aperiam animi voluptatem, cum tempore ex ipsam earum consequuntur, ducimus minus expedita accusamus deleniti cupiditate suscipit iusto harum! Etiam fermentum ex eget lorem ultricies, quis suscipit velit pulvinar. Donec lacinia orci at nisi maximus, eget lobortis ante luctus. Sed dignissim orci a nisl pulvinar consectetur.</p>
                <a href="<?php echo get_permalink(get_page_by_title('about us')); ?>" class = "read-more">Read more</a>
            </div>
            <div class = "col-sm-7 text-left" id = "full-width" style = "padding:0;">
                <img src="<?php bloginfo('template_url'); ?>/images/our-restaurant.jpg" alt="" class = "img-border">
                <!-- <h1 class = "lovely-restaurant">Our Lovely Restaurant</h1>
                <p class = "lovely-restaurant-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Sed maximus accumsan cursus. Proin pellentesque vulputate
                Duis aliquam lacus ultricies, sollicitudin metus ut. sed malesuada libero. In eget rhoncus ex. Proin vulputate nisl sed lacus posuere condimentum. In bibendum et libero non tincidunt. </p>
                <hr> -->
            </div>
        </div>
    </div>
</div>

<div class = "describe-restobox">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-sm-6 text-center no-padding">
                <img src="<?php bloginfo('template_url'); ?>/images/you-love-foods.jpg" alt="">
                <div class = "entry">
                    <h1 class = "entry-title">You Love Food?</h1>
                    <p class = "entry-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima ipsum sequi nostrum nesciunt ad eaque, dolore iure quibusdam eius explicabo nulla, ratione libero totam fugit? Facilis ipsum consequatur est nesciunt?</p>
                </div>
            </div>
            <div class = "col-sm-6 text-center no-padding">
                <img src="<?php bloginfo('template_url'); ?>/images/best-chefs.jpg" alt="">
                <div class = "entry">
                    <h1 class = "entry-title">We have the Best Chef</h1>
                    <p class = "entry-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima ipsum sequi nostrum nesciunt ad eaque, dolore iure quibusdam eius explicabo nulla, ratione libero totam fugit? Facilis ipsum consequatur est nesciunt?</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class = "our-lovely-menu">
    <div class = "container">
        <div class = "row">
            <div class = "col-sm-12 text-center">
                <h1 class = "lovely-menu-title">Our Lovely Menu</h1>
                <p class = "lovely-menu-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean nec ultricies leo, interdum sagittis nulla. Aliquam
                bibendum suscipit ipsum.</p>
            </div>
        </div>
        <div class = "row" id = "margin-menu">
            <div class = "col-sm-4 text-left">
                <img src="<?php bloginfo('template_url'); ?>/images/featured-menu-1.jpg" alt="">
                <h1 class = "featured-menu-title">Winter Hummos</h1>
                <p class = "featured-menu-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                n commodo vitae, semper sed lorem. Duis sodales quis
                massa sit amet ultrices.</p>
                <hr>
            </div>
            <div class = "col-sm-4 text-left menu-margin">
                <img src="<?php bloginfo('template_url'); ?>/images/featured-menu-2.jpg" alt="">
                <h1 class = "featured-menu-title">Brunch</h1>
                <p class = "featured-menu-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                n commodo vitae, semper sed lorem. Duis sodales quis
                massa sit amet ultrices.</p>
                <hr>
            </div>
            <div class = "col-sm-4 text-left menu-margin">
                <img src="<?php bloginfo('template_url'); ?>/images/featured-menu-3.jpg" alt="">
                <h1 class = "featured-menu-title">The Temptress</h1>
                <p class = "featured-menu-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                n commodo vitae, semper sed lorem. Duis sodales quis
                massa sit amet ultrices.</p>
                <hr>
            </div>
        </div>
        <div class = "row">
            <div class = "col-sm-12 text-center">
                <a href="<?php echo get_permalink(get_page_by_title('menu')); ?>" class = "btn btn-default">View Menu</a>
            </div>
        </div>
    </div>
</div>

<div class = "our-gallery">
    <div class = "container">
        <div class = "row">
            <div class = "col-sm-12 text-center">
                <h1 class = "our-gallery-title">Our Cool <span class = "gallery-color">Gallery</span></h1>
                <p class = "our-gallery-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero optio tempora nulla iure, neque excepturi. Cum repellendus praesentium.</p>    
            </div>
        </div>
    </div>
</div>
<div class = "gallery-links">
    <div class = "container">
        <div class = "row">
            <div class = "col-sm-12 text-center">
                <ul class = "nav justify-content-center">
                    <li class = "nav-item"><a href="#" class = "nav-link">All</a></li>
                    <li class = "nav-item"><a href="#" class = "nav-link">Dessert</a></li>
                    <li class = "nav-item"><a href="#" class = "nav-link">Breakfast</a></li>
                    <li class = "nav-item"><a href="#" class = "nav-link">Lunch</a></li>
                    <li class = "nav-item"><a href="#" class = "nav-link">Dinner</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class = "gallery-section">
    <div class = "container-fluid">
        <div class = "row margin-gallery">
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-1.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 1</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-2.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 2</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-3.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 3</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-4.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 4</h1>
                </div>
            </div>
        </div>
        <div class = "row margin-gallery">
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-5.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 5</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-6.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 6</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-7.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 7</h1>
                </div>
            </div>
            <div class = "col-md-3 text-center">
                <img src="<?php bloginfo('template_url'); ?>/images/gallery-8.jpg" alt="">
                <div class = "entry">
                    <h1 class = "gallery-title">Gallery 8</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class = "services">
    <div class = "container">
        <div class = "row">
            <div class = "col-md-4 text-left">
                <h1 class = "service-title"><span class = "numbers">01.</span> Cool Environment</h1>
                <img src="<?php bloginfo('template_url'); ?>/images/service-1.jpg" alt="">
                <p class = "service-desc">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit.  Curabitur sit amet diam
                Proin sit amet porttitor sapien.</p>
                <hr>
            </div>
            <div class = "col-md-4 text-left">
                <h1 class = "service-title"><span class = "numbers">02.</span> Freshest Food</h1>
                <img src="<?php bloginfo('template_url'); ?>/images/service-2.jpg" alt="">
                <p class = "service-desc">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit.  Curabitur sit amet diam
                Proin sit amet porttitor sapien.</p>
                <hr>
            </div>
            <div class = "col-md-4 text-left">
                <h1 class = "service-title"><span class = "numbers">03.</span> Most Delicious Food</h1>
                <img src="<?php bloginfo('template_url'); ?>/images/service-3.jpg" alt="">
                <p class = "service-desc">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit.  Curabitur sit amet diam
                Proin sit amet porttitor sapien.</p>
                <hr>
            </div>
        </div>
    </div>
</div>

<div class = "contact-us">
    <div class = "container">
        <div class = "row">
            <div class = "col-md-12 text-center">
                <h1 class = "contact-us-title"><span class = "contact">Contact</span> Us</h1>
                <p class = "contact-us-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis sapiente temporibus magnam.</p>
            </div>
        </div>
    </div>
</div>
<div class = "contact-us-forms">
    <div class = "container">
        <div class = "row">
            <div class = "col-md-6 text-left">
                <div class = "locate-us">
                    <h1 class = "resto-way">How to Locate Us</h1>
                    <p class = "resto-way-desc">Lorem ipsum dolor sit amet, consectetur
                    sed nunc aliquam varius. mi sodales in
                    Aenean in commodo purus.</p>
                </div>
                <div class = "our-address">
                    <h1 class = "resto-way">Our address</h1>
                    <p class = "resto-way-desc">
                    Restobox Inc. <br />
                    215-B Katipunan St.,<br />
                    Labangon, Cebu City
                    </p>
                </div>
            </div>
            <div class = "col-md-6 text-left">
                <h1 class = "resto-way">Reserve a Table</h1>
                <p class = "resto-way-desc">Lorem ipsum dolor sit amet, consectetur
                sed nunc aliquam varius. mi sodales in
                Aenean in commodo purus.</p>
                <div class = "form-contact">
                    <form>
                        <div class = "form-group">
                            <input type="text" class="form-control" id="Your-name" aria-describedby="emailHelp" placeholder="Your Name">
                        </div>
                        <div class = "form-group">
                            <input type="Email Address" class="form-control" id="email-address" aria-describedby="emailHelp" placeholder="Email Address">
                        </div>
                        <div class = "form-group">
                            <input type="date" class="form-control" id="reservation" aria-describedby="emailHelp" placeholder="Reservation Date">
                        </div>
                        <div class = "form-group">
                            <input type="text" class="form-control" id="number-people" aria-describedby="emailHelp" placeholder="Number of People">
                        </div>
                        <div class = "form-group">
                            <textarea class="form-control" id="Message" rows="5" placeholder="Message"></textarea>
                        </div>
                    </form>
                </div>
                <a href="#" class = "btn btn-default">Send</a>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

