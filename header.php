<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package restobox
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<header>
	<nav class = "navbar navbar-expand-lg fixed-top">
		<div class = "container">
			<a href="<?php esc_url(home_url('/') ); ?>" class = "navbar-brand"><?php bloginfo('name'); ?></a>
			<button class = "navbar-toggler" type = "button" data-toggle = "collapse" data-target = "#navbarSupportedContent" aria-controls = "navbarSupportedContent" aria-expanded = "false" aria-label = "Toggle Navigation">
				<span class = "navbar-toggler-icon"></span>
			</button>
			<div class = "collapse navbar-collapse" id = "navbarSupportedContent">
				<!-- <ul class = "navbar-nav ml-auto">
					<li class = "nav-item"><a href="#" class = "nav-link">Home</a></li>
					<li class = "nav-item"><a href="#" class = "nav-link">About us</a></li>
					<li class = "nav-item"><a href="#" class = "nav-link">Menu</a></li>
					<li class = "nav-item"><a href="#" class = "nav-link">Our Gallery</a></li>
					<li class = "nav-item"><a href="#" class = "nav-link">Contact us</a></li>
				</ul> -->
				<?php

					wp_nav_menu(array(
						'theme_location' => 'menu-1',
						'container' => 'ul',
						'menu_class' => 'navbar-nav ml-auto'
					));

				?>
			</div>
		</div>
	</nav>
</header>
